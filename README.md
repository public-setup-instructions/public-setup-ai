# ai-setup

1. You will need 3 machines with generally the following specifications:
   1. 16 Cores (48 Cores total)
   2. 128Gi Ram (384Gi Ram total)
   3. 1 disk for the OS and extra disks totaling 4tb (for a total of 2 disks for the OS and 12tb extra disks for OCS storage)
   4. At least one GPU (preferably an RTX 3080 or better)

2. Clear all disks of any partitions and put enough disks in each machine for Openshift Data Foundations (Openshift Container Storage). I prefer to have enough extra disks for at least 4tb Openshift Container Storage. So you will need 4tb of disk space (nvme, ssds, spinning disks) on physical devices separate from the drive the Red Hat CoreOS will be installed on. To format nvme drives I use an nvme to usb converter to plug in each device prior to install to format and clear them. You can also clear disks after Openshift is installed using sgdisk --zap-all
   1. If you're on windows:
       1. At the Run command type "diskpart"
       2. Once DiskPart starts run the following commands...
       3. list disk
       4. select disk <#>
       5. list partition
       6. sel partition <#> 
       7. delete partition override (repeat steps 6 and 7 until all partitions have been removed)
   2. You can also wait until Openshift is installed. In that case do the following for each node
       1. oc debug node/<node name>
       2. Find the device you want to clean and run the following commands...
       3. sgdisk --zap-all /dev/{devname}
3. Configure haproxy with haproxy.cfg. This is a file that shows how to use one load balancer to balance between multiple openshift clusters (even when installed using ipi and there exist internal haproxy instances on each cluster)
4. Go to cloud.redhat.com and follow assisted installer instructions. Make sure you have extra drives in each machine for at least 4tb of ODF storage. For 4tb you will need a total of 12tb in a condensed 3 node cluster
5. Create usb drives with discovery image
6. Once cluster is installed, set up ODF using Operator
7. Enable Registry with ODF storage
    1. oc edit configs.imageregistry.operator.openshift.io
    2. set Removed to Managed
    3. Create a new pvc with ocs-storagecluster-cephfs with at least 100gi of storage space
    	```yaml
           storage:
             pvc:
               claim: <pvc name>
    	```
8. Entitle the cluster using the 0003-cluster-wide-machineconfigs.yaml.template 
    Instructions from here: https://cloud.redhat.com/blog/how-to-use-entitled-image-builds-to-build-drivercontainers-with-ubi-on-openshift
    1. go to access.redhat.com
    2. subscriptions
    3. systems tab
    4. select a system with a valid subscription
    5. under "Subscriptions" tab click "Download Certificates"
    6. use the extracted pem file as the by replacting the {ID} in the following command with the extracted PEM file
        ```bash
           sed  "s/BASE64_ENCODED_PEM_FILE/$(base64 -w 0 {ID}.pem)/g" 0003-cluster-wide-machineconfigs.yaml.template > 0003-cluster-wide-machineconfigs.yaml
        ```
        
9. Install the Node Feature Discovery Operator
    1. Create an instance of Node Feature Discovery with no changes to yaml
10. Install the Nvidia GPU Operator
    1. Create an instance of ClusterPolicy with no changes to yaml
11. Install the Open Data Hub Operator
   	1. Create an instance of KDef. remove everything but jupyter notebook entries
   	2. Modify for the storage class as follows:  
   
	    ```yaml
		     - kustomizeConfig:
		        overlays:
		        - storage-class
		        parameters:
		        - name: storage_class
		          value: ocs-storagecluster-ceph-rbd
		        - name: s3_endpoint_url
		          value: "s3.odh.com"
		        repoRef:
		          name: manifests
		          path: jupyterhub/jupyterhub
		      name: jupyterhub
	    ```
    
    3. Add the cude overlay to trigger the build of the GPU enabled images. **ALERT** Due to a bug, once the buildconfigs are created, come back to this yaml file in the system and remove cuda or there will be a perpetual loop to building the cuda images
    https://github.com/opendatahub-io/odh-manifests/blob/master/jupyterhub/README.md
	    ```yaml
		      - kustomizeConfig:
		        overlays:
		        - cuda-11.0.3
		        - additional
		        repoRef:
		          name: manifests
		          path: jupyterhub/notebook-images
		      name: notebook-images
	    ```
 12. Spin up a notebook using tensorflow or pytorch and run the following to see that you have access to a GPU:
        ```
           import tensorflow as tf
           print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))
        ```   